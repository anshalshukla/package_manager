from sarge import run, Capture

from dependency_management.requirements.AnyOneOfRequirements import (
    AnyOneOfRequirements)
from dependency_management.requirements.ExecutableRequirement import (
    ExecutableRequirement)
from dependency_management.requirements.GoRequirement import (
    GoRequirement)
from dependency_management.requirements.PackageRequirement import (
    PackageRequirement)


class GoLangCiLinterRequirement(PackageRequirement):
    """
    This class is a subclass of ``PackageRequirement``. It requires
    GoLangCiLinter which installs multiple Go linter dependencies.
    """

    REQUIREMENTS = {
        AnyOneOfRequirements([
            GoRequirement('gopkg.in/golangci/golangci-lint.v1'),
            ExecutableRequirement('golangci-lint'),
        ])
    }

    _available_linter = {
        'asciicheck',
        'bodyclose',
        'cyclop',
        'deadcode',
        'depguard',
        'dogsled',
        'dupl',
        'durationcheck',
        'errcheck',
        'errorlint',
        'exhaustive',
        'exhaustivestruct',
        'exportloopref',
        'forbidigo',
        'forcetypeassert',
        'funlen',
        'gci',
        'gochecknoglobals',
        'gochecknoinits',
        'gocognit',
        'goconst',
        'gocritic',
        'gocyclo',
        'godot',
        'godox',
        'goerr113',
        'gofmt',
        'gofumpt',
        'goheader',
        'goimports',
        'gomnd',
        'gomoddirectives',
        'gomodguard',
        'goprintffuncname',
        'gosec',
        'gosimple',
        'govet',
        'ifshort',
        'importas',
        'ineffassign',
        'lll',
        'makezero',
        'misspell',
        'nakedret',
        'nestif',
        'nilerr',
        'nlreturn',
        'noctx',
        'nolintlint',
        'paralleltest',
        'prealloc',
        'predeclared',
        'promlinter',
        'revive',
        'rowserrcheck',
        'staticcheck',
        'structcheck',
        'stylecheck',
        'sqlclosecheck',
        'tagliatelle',
        'testpackage',
        'thelper',
        'tparallel',
        'typecheck',
        'unconvert',
        'unparam',
        'unused',
        'varcheck',
        'wastedassign',
        'whitespace',
        'wrapcheck',
        'wsl'
    }

    _install_command = None
    _is_installed = None

    def __init__(self, package='', version=''):
        """
        Constructs a new ``GoLangCiLinterRequirement``, using the
        ``PackageRequirement`` constructor.

        >>> pr = GoLangCiLinterRequirement('govet')
        >>> pr.type
        'golangci-lint'
        >>> pr.package
        'govet'

        :param package: A string with the name of the package to be installed.
        :param version: A version string. Leave empty to specify latest version.
        """
        if package not in GoLangCiLinterRequirement._available_linter:
            raise ValueError(
                package + ' is not available with GoLang Ci Linter.'
            )
        PackageRequirement.__init__(self, 'golangci-lint', package, version)

    def install_command(self):
        """
        Creates the installation command for the instance of the class.

        :param return: An array with the installation command.
        """
        if GoLangCiLinterRequirement._install_command is not None:
            return GoLangCiLinterRequirement._install_command

        GoLangCiLinterRequirement._install_command = [
            "curl -sSfL " +
            "https://raw.githubusercontent.com/golangci/golangci-lint/master/" +
            "install.sh | sh -s -- -b $(go env GOPATH)/bin v1.41.1"
            ]
        return GoLangCiLinterRequirement._install_command

    def install_package(self):
        """
        Runs the install command for the package given in a sub-process, and
        update the cached _is_install result.

        :param return: Returns exit code of running the install command
        """
        p = run(' '.join(self.install_command()),
                stdout=Capture(),
                stderr=Capture())
        GoLangCiLinterRequirement._is_installed = not p.returncode
        return p.returncode

    def is_installed(self):
        """
        Checks if the dependency is installed.

        :param return: True if dependency is installed, false otherwise.
        """
        if GoLangCiLinterRequirement._is_installed is not None:
            return GoLangCiLinterRequirement._is_installed

        env = run('go env GOPATH', stdout=Capture(), stderr=Capture())
        gopath = env.stdout.text.rstrip()
        p = run([gopath + '/bin/golangci-lint', '--enable --fast '],
                stdout=Capture(),
                stderr=Capture())
        GoLangCiLinterRequirement._is_installed = not p.returncode
        return GoLangCiLinterRequirement._is_installed
