import shutil
import unittest

from dependency_management.requirements.GemRequirement import GemRequirement


@unittest.skipIf(shutil.which('gem') is None,
                 "Gem is not installed.")
class GemRequirementTestCase(unittest.TestCase):

    def test__str__(self):
        self.assertEqual(str(GemRequirement('ruby')), 'ruby')
        self.assertEqual(str(GemRequirement('ruby', '3.0.1')), 'ruby 3.0.1')

    def test_not_installed_requirement(self):
        self.assertFalse(GemRequirement('some_bad_package').is_installed())
