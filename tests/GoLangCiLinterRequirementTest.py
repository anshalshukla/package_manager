import shutil
import unittest

from dependency_management.requirements.GoLangCiLinterRequirement import (
    GoLangCiLinterRequirement)


@unittest.skipIf(shutil.which('golangci-lint') is None,
                 'GoLangCiLinter is not installed or not in PATH.')
class GoLangCiLinterRequirementTestCase(unittest.TestCase):

    def test__str__(self):
        self.assertEqual(str(GoLangCiLinterRequirement('govet')), 'govet')

    def test_install_command(self):
        cmd = GoLangCiLinterRequirement('govet').install_command()
        self.assertIn('golangci-lint', cmd[0])

    def test_install_command_unavailable_package(self):
        with self.assertRaises(ValueError):
            GoLangCiLinterRequirement('some_bad_package').install_command()


@unittest.skipIf(shutil.which('golangci-lint') is None,
                 'GoLangCiLinter is not installed or not in PATH.')
class GoLangCiLinterInstallTestCase(unittest.TestCase):
    def test_install(self):
        self.assertFalse(GoLangCiLinterRequirement('govet').is_installed())
        self.assertEqual(GoLangCiLinterRequirement('govet').install_package(),
                         0)
        self.assertTrue(GoLangCiLinterRequirement('govet').is_installed())
        self.assertTrue(GoLangCiLinterRequirement('errcheck').is_installed())
