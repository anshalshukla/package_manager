Change log
================================================================================

0.5.0 - unreleased
--------------------------------------------------------------------------------

**updated**

#. `#142 <https://github.com/coala/dependency_management/issues/142>`_,
   Relicense as MIT

**deleted**

#. `#67 <https://github.com/coala/dependency_management/issues/67>`_, removed
   CabalRequirement

**added**

#. `#67 <https://github.com/coala/dependency_management/issues/67>`_, added
   HaskellRequirement with cabal and stack support

**added**

#. `#141 <https://github.com/coala/dependency_management/issues/141>`_, add
   AddressableRequirementSet

**added**

#. `#152 <https://github.com/coala/dependency_management/issues/152>`_, add
   bower support

**added**

#. `#122 <https://github.com/coala/dependency_management/issues/122>`_, add PEAR
   support

**added**

#. `#119 <https://github.com/coala/dependency_management/issues/119>`_, add
   MacPorts support

**added**

#. `#173 <https://github.com/coala/dependency_management/issues/173>`_, add
   FreeBSD pkg support

**added**

#. `#138 <https://github.com/coala/dependency_management/issues/138>`_, add gopm
   support

**added**

#. `#115 <https://github.com/coala/dependency_management/issues/115>`_, add perl
   CPAN support

**updated**

#. `#167 <https://github.com/coala/dependency_management/issues/167>`_,
   PackageRequirement.py: return exit code for install
#. `#117 <https://github.com/coala/dependency_management/issues/117>`_,
   RscriptRequirement.py: Fix typo
#. `#105 <https://github.com/coala/dependency_management/issues/105>`_,
   Helper.py: Fix typo
#. `#103 <https://github.com/coala/dependency_management/issues/103>`_,
   DistributionRequirement: Implement version check
#. `#5 <https://github.com/coala/dependency_management/issues/5>`_, Go and Julia
   and Rscript classes raise NotImplementedError if version supplied
#. `#125 <https://github.com/coala/dependency_management/issues/125>`_,
   GoRequirement: Deprecate flag
#. `#73 <https://github.com/coala/dependency_management/issues/73>`_,
   RScriptRequirement: Deprecate flag
