#!/bin/sh -xe

# Need $PYTHON

if [ "$DEBUG_DEPS" = "true" ]; then
  BASH="bash -ex"
else
  BASH=
fi

if [ "$PYTHON" = "python3.4" ]; then
  $PYTHON -m pip install --upgrade pip==19.1
fi
$PYTHON -m pip install --upgrade pip setuptools
npm install -g bower

if [ -f dependency_management/requirements/HaskellRequirement.py ]; then
  if [ -z "$(which stack)" ]; then
    curl -fsSL -o haskellstack.sh https://get.haskellstack.org/
    chmod +x haskellstack.sh
    $BASH ./haskellstack.sh
    stack setup --resolver ghc-8.4.2
  fi
fi
if [ -f dependency_management/requirements/CondaRequirement.py ]; then
  curl -fsSL -o miniconda.sh \
       http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
  chmod +x miniconda.sh
  $BASH ./miniconda.sh -bp "$HOME/miniconda"
fi
curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.41.1
cpan App::cpanminus
echo 'pytest-spec' >> test-requirements.txt
$PYTHON -m pip install -r requirements.txt -r test-requirements.txt

# install remotes package in R provided we want to test R functionality
if [ -f dependency_management/requirements/RscriptRequirement.py ]; then
  R -e "install.packages('remotes', repo='http://cran.rstudio.com', dependencies=TRUE)"
fi
