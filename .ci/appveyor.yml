image:
  - Visual Studio 2017
  - Visual Studio 2015

environment:
  global:
    PIP_CACHE_DIR: C:\pip_cache
    PIP_DISABLE_PIP_VERSION_CHECK: 1
    # Needed if pip uninstall is used
    PIP_YES: 1
    YARN_GPG: 'no'
    FudgeCI: $(APPVEYOR_BUILD_FOLDER)\.ci
    NUGET_EXE_NO_PROMPT: 1
    NUGET_HTTP_CACHE_PATH: C:\nuget_http_cache
    CHOCO_CACHE_DIR: C:\choco_cache
    MSYS_ROOT: C:\msys64
    MSYS_BIN: $(MSYS_ROOT)\usr\bin
    MINGW_BIN: C:\MinGW\bin
    JAVA_HOME: C:\jdk
    MSSDK_ROOT: C:\Program Files\Microsoft SDKs
    VS_ROOT: C:\Program Files (x86)\Microsoft Visual Studio
    # TODO: Use choco vswhere to locate suitable compiler
    # https://github.com/microsoft/vswhere/issues/187
    WIN71_SDK_ROOT: $(MSSDK_ROOT)\Windows\v7.1
    VS14_VC: $(VS_ROOT) 14.0\VC
    VS2017_VC: $(VS_ROOT)\2017\Community\VC
    GOPATH: C:\gopath
    R_COMPILE_AND_INSTALL_PACKAGES: never
    # R compiler path
    BINPREF: $(MINGW_BIN)\
    # A problem with store_env_in_registry.py means this needs to be explicit
    PSModulePath: >-
      $(PSModulePath);C:\Program Files (x86)\WindowsPowerShell\Modules;
    PATH: >-
      C:\python;C:\python\Scripts;$(PATH);$(MINGW_BIN);$(MSYS_BIN);
      C:\tools\fudge\tools;
      C:\ruby\bin;
      C:\jdk\bin;
      C:\miniconda;C:\miniconda\Scripts;
      $(GOPATH)\bin;
      $(APPVEYOR_BUILD_FOLDER)\node_modules\.bin;
      $(APPVEYOR_BUILD_FOLDER)\vendor\bin;

  matrix:
    - PYTHON_VERSION: 3.5
    - PYTHON_VERSION: 3.6
    - PYTHON_VERSION: 3.7

platform:
  - x86
  - x64

cache:
  - C:\nuget_http_cache
  - C:\choco_cache
  - "C:\\pip_cache"
  - "node_modules"
  - "C:\\Users\\appveyor\\AppData\\Local\\coala-bears\\coala-bears"
  - "C:\\Users\\appveyor\\AppData\\Roaming\\nltk_data"
  - "%LOCALAPPDATA%\\Composer"
  - C:\Users\appveyor\AppData\Roaming\cabal\

branches:
  except:
    - /^sils\/.*/

# This forces unix style line endings in the clone, which is necessary to
# avoid warning regarding EOLs when running git diff on Windows
init: git config --global core.autocrlf false

install:
  # Show initial state
  - powershell -c "$PSVersionTable"
  # Uncomment to debug
  # printenv
  - python --version
  - "python -c \"import struct; print(struct.calcsize('P') * 8)\""
  - python -m pip --version
  - python -c "import setuptools; print(setuptools.__version__)"
  - node --version
  - which npm
  - npm --version
  - npm config get prefix
  - ruby --version
  - bundler --version
  - go version
  - which java
  - java -version
  - which javac
  - javac -version
  # JAVA_HOME set above is not populated yet
  - echo %JAVA_HOME%
  - ls %JAVA_HOME%/bin/java & exit 0
  - which perl
  - perl --version
  - which gcc
  - gcc --version
  - which mingw32-make.exe & exit 0
  - mingw32-make.exe --version & exit 0

  - .ci\vs_appveyor.cmd
  - which cl
  # cl.exe often exits with non-zero exit status when given no arguments
  - cl & exit 0

  # Stores environment in registry, with minor tweaks
  - python .ci/store_env_in_registry.py
  - refreshenv

  # Set up AppVeyor product versions, and install dummy choco entries for them
  - ps: . .ci/FudgeCI.ps1; Initialize-AppVeyorVM
  - refreshenv
  - echo %PATH%

  # Show updated SOE; versions should be as defined in top of the Fudgefile
  - python --version
  - "python -c \"import struct; print(struct.calcsize('P') * 8)\""
  - node --version
  - which npm
  - npm --version
  - npm config get prefix
  - go version
  - ruby --version
  - which java
  - java -version
  - which javac
  - javac -version
  - ls %JAVA_HOME%/bin/java
  - perl --version
  - which ppm
  - ppm --version
  - which gcc
  - gcc --version
  - which mingw32-make.exe
  - mingw32-make.exe --version
  - conda --version

  - "%MSYS_BIN%\\date.exe"
  - cp .ci/choco.config
      %ChocolateyInstall%\config\chocolatey.config
  # Install remainer of the Fudgefile with chocolatey using Fudge
  - ps: . .ci/Fudge.ps1 install
  - refreshenv
  - echo %PATH%

  - ps: if ($env:APPVEYOR_JOB_NUMBER -eq 1) { . .ci/PSLint.ps1 }

  # Check that we have the expected version and architecture for Python
  - "python --version"
  - "python -c \"import struct; print(struct.calcsize('P') * 8)\""
  # Confirm other versions
  - node --version
  - which npm
  - npm --version
  - npm config get prefix
  - go version
  - ruby --version
  - perl --version
  - which gcc
  - gcc --version
  - which mingw32-make.exe
  - mingw32-make.exe --version
  - which java
  - java -version
  - which javac
  - javac -version
  - ls %JAVA_HOME%/bin/java
  - conda --version
  # Newly installed versions
  - R --version
  - which bower & exit 0
  - bower --version & exit 0
  - which composer & exit 0
  - composer --version & exit 0
  - ppm --version & exit 0


  - "%MSYS_BIN%\\date.exe"

build: false  # Not a C# project, build stuff at the test step instead.

test_script:
  - python -m pip --version
  - python -c "import setuptools; print(setuptools.__version__)"

  - py.test

  - if not "%PYTHON_VERSION%" == "3.7"
      python setup.py install

  - python -m pip install
      git+https://github.com/coala/coala#egg=coala
      git+https://github.com/coala/coala-bears#egg=coala-bears


  - coala --ci

on_success:
  - codecov

on_failure:
  - codecov

matrix:
  fast_finish: true
