Set-StrictMode -Version latest

function Install-GoLangCiLinter {
    curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.41.1
}

function Complete-Install {
    Install-GoLangCiLinter
}

Export-ModuleMember -Function Install-GoLangCiLinter, Complete-Install
